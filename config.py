SECRET_KEY = "XXXXXXXXXXXX Random junk XXXXXXXXXXXXXXXXX"
KEBOOLA_TOKEN = "YYYYYYYYYYYYYYYYYY"
TEMPLATES_AUTO_RELOAD = True
EXPLAIN_TEMPLATE_LOADING = False

MODEL_RUNNER = {
    'transformation_id': '301649164',
    'bucket_id': '321925419'
}

MODEL_RUNNER_PRODUCTION = {
    'transformation_id':'301649164',
    'bucket_id':'316804304'
}

USERS = {  # /auth/manual hard-coded users
    'admin': {
        'name': 'Admin',
        'pwd': '<correct-horse-battery-staple-hard-to-guess-plaintext-password>',
        #
        # categories
        #
        # You can specify categories just like before:
        #
        # 'categories': [('cat_id', 'cat_name'), ...]
        #
        # If you don't specify any categories, Admin will see ALL CATEGORIES
    },
}
