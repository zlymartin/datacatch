import requests
from requests.auth import HTTPBasicAuth
import json
import pprint

import flask
import flask_login
import json

app = flask.Flask(__name__)
app.config.from_pyfile(app.root_path + '/config.py')
app.jinja_env.add_extension('pypugjs.ext.jinja.PyPugJSExtension')

url = "https://vh668.ipex.cz/api/calls/"
API_KEY = "5b92ba3b0563f1a8b768fe75"
API_SECRET = "b9e02d6ee621/4508afd9/520a007a7324"
time_data = "?startTime=2018-02-04%2010%3A00%3A00&endTime=2018-07-10%2011%3A00%3A00"

@app.route('/')

def index():
    dropped_call_data = "NONE"
    if 1 == 1:
        resp = requests.get(
            url + time_data,
            auth=HTTPBasicAuth(API_KEY, API_SECRET),
        )

        print resp
        data = resp.text
        json_data = json.loads(resp.text)

        pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(json_data)
        print "Total matching (w double matching):", len([x for x in json_data['items'] if x['disposition']=='FAILED' and x['line']!=0])

        i = 0
        dropped_call_data = [x for x in json_data['items'] if x['disposition'] == 'FAILED' and x['line'] != 0 and x['uri'] != '']
        call_centre_data = [x for x in json_data['items'] if x['disposition'] != 'FAILED' and x['line'] != 0]

        dropped_call_data2 = []

        print "DROPPED:"
        for row in dropped_call_data:
            i = i + 1
            print i, row['stamp']," value: ", str(row['line'])[1:], " by nr.: ", row['clid'], row['calldate']

            nr_start_position = row['clid'].find("<")
            if nr_start_position != -1:
                row['clid'] = row['clid'][nr_start_position+1:-1]
                phone_nr_length = len(row['clid'])

                xes = ""
                for _ in range(phone_nr_length-4):
                    xes = xes + "X"
                    print xes
                row['clid'] = row['clid'][0:2] + xes + row['clid'][phone_nr_length-2:]
                print row['clid']
                row['line']=str(row['line'])[1:]
            #dropped_call_data2.append(i, row['stamp']," value: ", str(row['line'])[1:], " by nr.: ", row['clid'], row['calldate'])

        print "CALL CENTRE:"
        for row in call_centre_data :
            i = i + 1
            print i, row['stamp'], " value: ", str(row['line'])[1:], " by nr.: ", row['clid'], row['calldate']

    return flask.render_template('main_page.pug', data=dropped_call_data)
####################################################################################################
# SERVER START

if __name__ == '__main__':
    app.run(port=32767, host="0.0.0.0", debug=True, threaded=True)
